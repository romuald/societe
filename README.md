# Société

## Environnement
- [Eclairages de ministère de l'écologie](https://www.ecologie.gouv.fr/politiques/comprendre-changement-climatique)
  - [Carte des impacts en 2050](https://www.ecologie.gouv.fr/sites/default/files/18140_PNACC2_infog_V3-810.png) issue de [l'article de l'Observatoire National sur Effets du Réchauffement Climatique](https://www.ecologie.gouv.fr/observatoire-national-sur-effets-du-rechauffement-climatique-onerc)
  - [Impacts ](https://www.ecologie.gouv.fr/impacts-du-changement-climatique-sante-et-societe)
- [Centre National de la Recherche Scientifique : Environnement](https://lejournal.cnrs.fr/terre)
- [Bases de données des géorisques français](https://www.georisques.gouv.fr/donnees/bases-de-donnees)
- [Carte des alternatives écologiques et sociales](https://transiscope.org/)
- [Colibris : mouvement d'actions citoyennes](https://www.colibris-lemouvement.org/)
- [Actualités écologiques](https://reporterre.net/)

### Vidéos et audios
- [Impact de l'agriculture, notamment de l'élevage](https://www.osonscauser.com/faut-arreter-viande-sauver-climat/) agneau et boeuf les plus émetteurs de CO2/kg
- [Impact du secteur du bâtiment, notamment du béton et du sable](https://www.youtube.com/watch?v=O9h0NCzF_D8)
- [Les apports colossaux des énergies fossibles](https://www.osonscauser.com/merci-energies-fossiles-comprendre-probleme-de-lenergie/)
  - L'espérance de vie de 35 ans à 80 ans
  - L'augmentation fulgurante de productivité, notamment dans le secteur agricole
  - Le temps gagné grâce aux machines, par exemple le lave-linge
- [Complément de la vision historique des dépendances énergétiques](https://www.youtube.com/watch?v=S3JawPcTYg0)
- Emma Haziza hydrologue de [Mayane](https://mayane.eu/fr/accueil/) sur la problématique de la gestion de l'eau [podcast](https://www.thinkerview.com/podcast-download/32074/emma-haziza-crise-de-leau-planete-terre-invivable.mp3?ref=new_window)/[vidéo](https://videos.thinkerview.com/w/uuXSjDo8YopuxDn3j3D6Be)
- Aurore Stephant géologue minière de [Systex](https://www.systext.org/) sur la problématique de notre dépendance aux métaux [podcast](https://www.thinkerview.com/podcast-download/30094/aurore-stephant-leffondrement-le-point-critique.mp3?ref=new_window)/[vidéo](https://videos.thinkerview.com/videos/watch/dee86403-b12b-42c3-8157-04f29bb117b9)
  - Questionne l'utilisation des voitures électriques qui utilisent plus de métaux
- Anaïs Voy Gillis docteur en géographie sur la réindustrialisation et la problématique des transports [podcast](https://www.thinkerview.com/podcast-download/28371/anais-voy-gillis-reindustrialisation-reprendre-son-autonomie.mp3?ref=new_window)/[vidéo](https://videos.thinkerview.com/w/oecfuVhN2MzWbsGJyCHbUj)
  - Propose d'accompagner les fonderies vers les voitures électriques
